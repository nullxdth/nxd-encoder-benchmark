(in-package #:cl-x264-tune)

(defconstant +default-bitrate+
  8000)

(defconstant +default-preset+
  :medium)

(defconstant +default-keyframe-interval+
  2)

(defconstant +default-profile+
  nil)

(defconstant +default-tune+
  nil)

(defmacro hash-table-fill (ht &body pairs)
  (assert (evenp (length pairs)))
  (let ((htname (gensym "HT")))
    (labels ((filler (list acc)
               (if list
                   (destructuring-bind (k v . rest) list
                     (filler rest
                             `(,v (gethash ,k ,htname) ,@acc)))
                   (nreverse acc))))
      `(let ((,htname ,ht))
         (setf ,@(filler pairs nil))
         ,htname))))

(defun read-batch (stream)
  (let ((table (make-hash-table :test #'equal)))
    (loop
      (destructuring-bind (key value) (split "=" (read-line stream))
        (setf (gethash key table) value)
        (when (string= key "progress")
          (return-from read-batch (values table (string= value "end"))))))))

(defun ffprobe/nb-frames (path)
  (let ((output (with-output-to-string (stdout)
                  (run-program
                   "ffprobe"
                   `("-v" "error"
                          "-select_streams" "v:0"
                          "-show_entries" "stream=nb_frames"
                          "-of" "default=noprint_wrappers=1:nokey=1"
                          ,path)
                   :search t
                   :wait t
                   :window :hide
                   :output stdout))))
    (parse-integer output)))

(defun extract-bitrate (bitrate-string)
  (multiple-value-bind (match groups)
      (scan-to-strings "(.+)kbits/s" bitrate-string)
    (if match
        (read-from-string (elt groups 0))
        0)))

(defun extract-speed (speed-string)
  (read-from-string (string-right-trim '(#\x) speed-string)))

(defun format/fps (fps)
  (if (= fps most-positive-single-float)
      ""
      (format nil "~2$" fps)))

(defun format/bitrate (bitrate)
  (format nil "~1$" bitrate))

(defun format/speed (speed)
  (format nil "~2$" speed))

(defun parse-x264-params (params)
  (substitute #\: #\Space params))

(defun ffmpeg/x264 (path &key
                           (fps 60)
                           (bitrate +default-bitrate+)
                           (bufsize (* 2 bitrate))
                           (keyframe-interval +default-keyframe-interval+)
                           (preset  +default-preset+)
                           (profile +default-profile+)
                           (tune    +default-tune+)
                           (params  ""))
  (let* ((bitrate (format nil "~aK" bitrate))
         (bufsize (format nil "~aK" bufsize))
         (params  (format nil
                          "nal-hrd=cbr:keyint=~a:~a"
                          (* keyframe-interval fps)
                          (parse-x264-params params)))
         (1% (/ (ffprobe/nb-frames path) 100))
         (p (run-program
             "ffmpeg"
             `("-y"
               "-loglevel" "quiet"
               "-i" ,path
               "-c:v" "libx264"
               "-preset" ,(string-downcase (string preset))
               ,@(when profile
                   (list "-profile" (string-downcase (string profile))))
               ,@(when tune
                   (list "-tune" (string-downcase (string tune))))
               "-x264-params" ,params
               "-b:v"     ,bitrate
               "-minrate" ,bitrate
               "-maxrate" ,bitrate
               "-bufsize" ,bufsize
               "-progress" "-"
               "-stats_period" "0.2"
               "-f" "mp4" "NUL"
               ;; "-f" "null" "-"
               )
             :search t
             :wait nil
             :window :hide
             :output :stream))
         (output (process-output p)))
    (let ((min-fps     most-positive-single-float)
          (min-bitrate most-positive-single-float)
          (min-speed   most-positive-single-float)
          (max-fps     most-negative-single-float)
          (max-bitrate most-negative-single-float)
          (max-speed   most-negative-single-float)
          (sum-fps     0)
          (sum-bitrate 0)
          (sum-speed   0)
          (i           0))
      (loop
        (multiple-value-bind (batch endp) (read-batch output)
          (let ((cur-frame (parse-integer (gethash "frame" batch)))
                (cur-fps (read-from-string (gethash "fps" batch)))
                (cur-bitrate (extract-bitrate (gethash "bitrate" batch)))
                (cur-speed (extract-speed (gethash "speed" batch))))
            (let* ((current (hash-table-fill (make-hash-table :test #'equal)
                              "fps"     (format/fps     cur-fps)
                              "bitrate" (format/bitrate cur-bitrate)
                              "speed"   (format/speed   cur-speed)))
                   (minimum (make-hash-table :test #'equal))
                   (maximum (make-hash-table :test #'equal))
                   (average (make-hash-table :test #'equal))
                   (table (hash-table-fill (make-hash-table :test #'equal)
                            "frame"     cur-frame
                            "progress" (round cur-frame 1%)
                            "current"   current
                            "minimum"   minimum
                            "maximum"   maximum
                            "average"   average)))
              (incf i)
              (incf sum-fps      cur-fps)
              (incf sum-bitrate  cur-bitrate)
              (incf sum-speed    cur-speed)
              (unless (zerop cur-fps)
                (setf min-fps (min min-fps cur-fps)))
              (setf min-bitrate (min min-bitrate cur-bitrate)
                    min-speed   (min min-speed   cur-speed)
                    max-fps     (max max-fps     cur-fps)
                    max-bitrate (max max-bitrate cur-bitrate)
                    max-speed   (max max-speed   cur-speed))
              (hash-table-fill minimum
                "fps"     (format/fps     min-fps)
                "bitrate" (format/bitrate min-bitrate)
                "speed"   (format/speed   min-speed))
              (hash-table-fill maximum
                "fps"     (format/fps     max-fps)
                "bitrate" (format/bitrate max-bitrate)
                "speed"   (format/speed   max-speed))
              (hash-table-fill average
                "fps"     (format/fps     (/ sum-fps i))
                "bitrate" (format/bitrate (/ sum-bitrate i))
                "speed"   (format/speed   (/ sum-speed i)))
              (format t "~&~a" (jonathan:to-json table))))
          (when endp
            (terpri)
            (return)))))
    (process-wait p)))

(defun ensure-integer (object)
  (etypecase object
    (integer object)
    (string (parse-integer object))))

(defun main ()
  (let ((args (cl-argparse:parse
               (cl-argparse:create-main-parser
                   (main-parser "nullxdth encoder benchmark")
                 (cl-argparse:add-optional
                  main-parser
                  :short "b"
                  :long "bitrate"
                  :help "Bitrate"
                  :default +default-bitrate+
                  :var "bitrate")
                 (cl-argparse:add-optional
                  main-parser
                  :short "k"
                  :long "keyint"
                  :help "Keyframe interval (seconds)"
                  :default +default-keyframe-interval+
                  :var "keyint")
                 (cl-argparse:add-optional
                  main-parser
                  :short "p"
                  :long "preset"
                  :help "x264 encoding preset"
                  :default +default-preset+
                  :var "preset")
                 (cl-argparse:add-optional
                  main-parser
                  :short "r"
                  :long "profile"
                  :help "x264 encoding profile"
                  :default +default-profile+
                  :var "profile")
                 (cl-argparse:add-optional
                  main-parser
                  :short "t"
                  :long "tune"
                  :help "x264 encoding tune"
                  :default +default-tune+
                  :var "tune")
                 (cl-argparse:add-optional
                  main-parser
                  :short "x"
                  :long "params"
                  :help "x264 encoding parameters"
                  :default ""
                  :var "params"))
               (rest sb-ext:*posix-argv*))))
    (ffmpeg/x264
     "c:/benchmark/input.mp4"
     :bitrate (ensure-integer (cl-argparse:get-value "bitrate" args))
     :keyframe-interval (ensure-integer (cl-argparse:get-value "keyint" args))
     :preset  (cl-argparse:get-value "preset"  args)
     :profile (cl-argparse:get-value "profile" args)
     :tune    (cl-argparse:get-value "tune"    args)
     :params  (cl-argparse:get-value "params"  args))))

;; (ffmpeg/x264 "c:/benchmark/input.mp4")

;; (main)
