(asdf:load-system "cl-csv")
(asdf:load-system "cl-ppcre")
(asdf:load-system "jonathan")
(asdf:load-system "serapeum")

(defun parse-output (output)
  (mapcar (lambda (batch)
            (let ((table (make-hash-table :test #'equal)))
              (dolist (line (cl-ppcre:split "\\n" batch) table)
                (destructuring-bind (key value) (cl-ppcre:split "=" line :limit 2)
                  (setf (gethash key table) value)))))
          (cl-ppcre:split "\\n{3,}"
                          (string-trim '(#\Newline)
                                       (remove #\Return output)))))

(defun wmic (alias &optional fields)
  (let ((output (with-output-to-string (stdout)
                  (run-program "wmic"
                               `(,alias "get" ,(format nil "~{~a~^,~}" fields) "/format:list")
                               :search t
                               :wait t
                               :output stdout))))
    (parse-output output)))

(defun get-memorychip-info ()
  (mapcar (lambda (table)
            (format nil "~a ~a ~a ~aGb ~aMHz"
                    (gethash "DeviceLocator" table)
                    (gethash "Manufacturer" table)
                    (gethash "PartNumber" table)
                    (/ (parse-integer (gethash "Capacity" table))
                       (expt 1024 3))
                    (gethash "ConfiguredClockSpeed" table)))
          (wmic "memorychip"
                '("Manufacturer"
                  "PartNumber"
                  "Capacity"
                  "ConfiguredClockSpeed" ; "Speed"
                  "DeviceLocator"))))

(defun get-cpu-info ()
  (mapcar (lambda (table)
            (format nil "~a ~a/~a"
                    (string-trim '(#\Space) (gethash "Name" table))
                    (gethash "NumberOfCores" table)
                    (gethash "NumberOfLogicalProcessors" table)))
          (wmic "cpu"
                '("Name"
                  "NumberOfCores"
                  "NumberOfLogicalProcessors"))))

(defun get-hardware-info ()
  (let ((cpu (get-cpu-info))
        (memorychip (get-memorychip-info)))
    (let ((table (make-hash-table :test #'equal)))
      (setf (gethash "cpu" table) cpu
            (gethash "memorychip" table) memorychip)
      (values table))))

(defun main ()
  (format t "~a" (jonathan:to-json (get-hardware-info))))

(defun build ()
  (save-lisp-and-die "nxd-encoder-benchmark.exe"
                     :executable t
                     :purify t
                     :toplevel #'main))
