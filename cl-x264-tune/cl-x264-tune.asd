(in-package #:cl-user)

(defpackage #:cl-x264-tune-asd
  (:use #:cl #:asdf))

(in-package #:cl-x264-tune-asd)

(defsystem #:cl-x264-tune
  :description "x264 benchmark & tune utility"
  :version "0.0.1"
  :author "Andrei V. Tikhonov <nullxdth@gmail.com>"
  :depends-on (#:cl-ppcre
               #:jonathan
               #:cl-argparse)
  :components ((:module "src"
                :components ((:file "package")
                             (:file "ffmpeg-wrapper")))))
