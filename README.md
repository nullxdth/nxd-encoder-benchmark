# nxd-encoder-benchmark

# Architecture Idea
```mermaid
flowchart TB
    x264[x264 Benchmark: Common Lisp] <-- command args/STDOUT --> ffmpeg
    x264 <-- Hardware information --> wmic
    Video[Input Video File] --> ffmpeg
    x264 --> OS[WMI: CPU/RAM/Windows version info]
    x264 -- HTTP API - info:\nresult,\nhardware,\nx264 settings --> Stat((Benchmark Statistic Server))
    UI[UI: Tcl/Tk] <-- command args/STDOUT --> x264
```
