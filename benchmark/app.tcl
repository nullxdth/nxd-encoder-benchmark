lappend auto_path [file join [file dirname [info script]] lib]

option readfile app-layout.rc
option readfile app-en.rc

package require benchmark

proc geometry {window width height} {
  set x [expr ([winfo screenwidth  $window] - $width ) / 2]
  set y [expr ([winfo screenheight $window] - $height) / 2]
  wm geometry $window ${width}x${height}+$x+$y
}

proc main {} {
  wm title     . {x264 Stream Tuner v0.1}
  wm resizable . 1 0
  geometry     . 800 430

  ::benchmark::makeSettingsWidgets  bitrate keyint preset profile tune params
  ::benchmark::makeStatsWidgets
  ::benchmark::makeBenchmarkWidgets bitrate keyint preset params

  grid .lfSettings  -sticky news -padx 8
  grid .lfStats     -sticky news -padx 8
  grid .lfBenchmark -sticky news -padx 8

  grid columnconfigure . 0 -weight 1

  . configure -menu [::benchmark::makeMenu]

  # bind . <Destroy> ::benchmark::writeConfig

  focus -force .
}

main
