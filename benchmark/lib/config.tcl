package provide benchmark 1.0

package require json
package require json::write

namespace eval benchmark {
  proc writeConfig {} {
    set geometry [wm geometry .]
    set jsondata [json::write::object geometry [json::write::string $geometry]]

    set f [open benchmark.json w]
    puts $f $jsondata
    close $f
  }
}
