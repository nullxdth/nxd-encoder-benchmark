package provide benchmark 1.0

package require json

namespace eval benchmark {
  variable presets {
    ultrafast
    superfast
    veryfast
    faster
    fast
    medium
    slow
    slower
    veryslow
    placebo
  }
  variable profiles {
    (none)
    baseline
    main
    high
  }
  variable tunes {
    (none)
    film
    animation
    grain
    stillimage
    psnr
    ssim
    fastdecode
    zerolatency
  }

  variable defaultBitrate 8000
  variable defaultKeyInt  2
  variable defaultPreset  ultrafast
  variable defaultProfile (none)
  variable defaultTune    (none)
  variable defaultOptions {threads=32 lookahead-threads=16 rc-lookahead=60 ref=3 brames=2 trellis=0}
}

proc ::benchmark::centerByScreen {window width height} {
  set x [expr {([winfo screenwidth  .] - $width)  / 2}]
  set y [expr {([winfo screenheight .] - $height) / 2}]
  wm geometry $window ${width}x${height}+$x+$y
}

proc ::benchmark::dlgSettings {} {
  toplevel .dlgSettings

  wm withdraw .dlgSettings

  ::benchmark::centerByScreen .dlgSettings 640 480

  wm title .dlgSettings Settings
  wm resizable .dlgSettings 0 0
  wm transient .dlgSettings .

  ttk::label .dlgSettings.lLanguage -text Language
  ttk::combobox .dlgSettings.cbLanguage -value {English Russian} -state readonly
  .dlgSettings.cbLanguage set English

  bind .dlgSettings.cbLanguage <<ComboboxSelected>> {
    set table [dict create English app-en.rc Russian app-rua.rc]
    set rcFilename [dict get $table [%W get]]
  }

  grid .dlgSettings.lLanguage .dlgSettings.cbLanguage -sticky news

  wm deiconify .dlgSettings

  focus .dlgSettings
  grab  .dlgSettings

  tkwait window .dlgSettings
}

proc ::benchmark::makeMenu {} {
  set m [menu .menubar]

  $m add cascade -label File -menu $m.file
  $m add cascade -label Help -menu $m.help

  menu $m.file -tearoff 0
  $m.file add command -label Settings -command ::benchmark::dlgSettings
  $m.file add separator
  $m.file add command -label Quit -command {destroy .}

  menu $m.help -tearoff 0
  $m.help add command -label About

  return $m
}

proc ::benchmark::run {bitrate keyint preset params} {
  set args [list |../benchmark.exe --bitrate $bitrate --keyint $keyint --preset $preset --params $params]
  set pipe [open $args]
  fileevent $pipe readable [list ::benchmark::reader $pipe]
  fconfigure $pipe -blocking 0
}

proc ::benchmark::reader {pipe} {
  global frameNumber

  set line [gets $pipe]
  if {$line eq "" || [eof $pipe]} {
    ::benchmark::changeWidgetsState !disabled
    close $pipe
  } else {
    set batch [::json::json2dict $line]
    set current [dict get $batch current]
    set maximum [dict get $batch maximum]
    set minimum [dict get $batch minimum]
    set average [dict get $batch average]

    ::benchmark::setProgress [dict get $batch progress]

    dict with current {::benchmark::setStats Cur $fps $bitrate $speed}
    dict with minimum {::benchmark::setMinStats $fps $bitrate $speed}
    dict with maximum {::benchmark::setStats Max $fps $bitrate $speed}
    dict with average {::benchmark::setStats Avg $fps $bitrate $speed}

    set frameNumber [dict get $batch frame]
  }
}

proc ::benchmark::setMinStats {fps bitrate speed} {
  global FpsMin
  global BitrateMin
  global SpeedMin

  set FpsMin $fps
  set BitrateMin $bitrate
  set SpeedMin $speed

  .lfStats.lFpsMin   configure -foreground [expr {$FpsMin   < 60 ? {red} : {green}}]
  .lfStats.lSpeedMin configure -foreground [expr {$SpeedMin <  1 ? {red} : {green}}]
}

proc ::benchmark::setStats {suffix fpsValue bitrateValue speedValue} {
  upvar #0 Fps$suffix fps
  upvar #0 Bitrate$suffix bitrate
  upvar #0 Speed$suffix speed

  set fps $fpsValue
  set bitrate $bitrateValue
  set speed $speedValue
}

proc ::benchmark::makeStatsWidgets {} {
  set f [ttk::labelframe .lfStats]

  ttk::label $f.lFps     -class TLabelRow
  ttk::label $f.lBitrate -class TLabelRow
  ttk::label $f.lSpeed   -class TLabelRow
    
  ttk::label $f.lCur -class TLabelCol
  ttk::label $f.lMin -class TLabelCol
  ttk::label $f.lMax -class TLabelCol
  ttk::label $f.lAvg -class TLabelCol

  foreach row {Fps Bitrate Speed} {
    foreach col {Cur Min Max Avg} {
      ttk::label $f.l$row$col -textvariable $row$col -class TLabelCell
    }
  }

  grid           x $f.lCur        $f.lMin        $f.lMax        $f.lAvg        -sticky news
  grid $f.lFps     $f.lFpsCur     $f.lFpsMin     $f.lFpsMax     $f.lFpsAvg     -sticky news
  grid $f.lBitrate $f.lBitrateCur $f.lBitrateMin $f.lBitrateMax $f.lBitrateAvg -sticky news
  grid $f.lSpeed   $f.lSpeedCur   $f.lSpeedMin   $f.lSpeedMax   $f.lSpeedAvg   -sticky news

  grid columnconfigure $f {0 1 2 3 4} -weight 1
}

proc ::benchmark::setProgress {progressValue} {
  global progress
  set progress $progressValue
}

proc ::benchmark::makeBenchmarkWidgets {bitrateVarName
                                        keyintVarName
                                        presetVarName
                                        paramsVarName} {
  upvar #0 $bitrateVarName bitrate
  upvar #0 $keyintVarName  keyint
  upvar #0 $presetVarName  preset
  upvar #0 $paramsVarName  params

  ttk::labelframe .lfBenchmark
  ttk::label .lfBenchmark.lFrame -textvariable frameNumber
  ttk::progressbar .lfBenchmark.pProgress -orient horizontal -mode determinate -variable progress
  ttk::button .lfBenchmark.bRun -command {
    ::benchmark::changeWidgetsState disabled
    ::benchmark::run $bitrate $keyint $preset $params
  }

  grid .lfBenchmark.lFrame  .lfBenchmark.pProgress -sticky news -pady {4 0}
  grid x                    .lfBenchmark.bRun      -sticky e    -pady {4 0}

  grid columnconfigure .lfBenchmark 0 -weight 0
  grid columnconfigure .lfBenchmark 1 -weight 1
}

proc ::benchmark::makeSettingsWidgets {bitrateVarName
                                       keyintVarName
                                       presetVarName
                                       profileVarName
                                       tuneVarName
                                       paramsVarName} {

  upvar #0 $paramsVarName params

  ttk::labelframe .lfSettings

  ttk::label .lfSettings.lBitrate
  ttk::spinbox .lfSettings.sbBitrate -from 0 -to 50000 -increment 50 -textvariable $bitrateVarName
  .lfSettings.sbBitrate set $::benchmark::defaultBitrate

  ttk::label .lfSettings.lKeyInt
  ttk::spinbox .lfSettings.sbKeyInt -from 0 -to 20 -increment 1 -textvariable $keyintVarName
  .lfSettings.sbKeyInt set $::benchmark::defaultKeyInt

  ttk::label .lfSettings.lPreset
  ttk::combobox .lfSettings.cbPreset -value $::benchmark::presets -textvariable $presetVarName
  .lfSettings.cbPreset set $::benchmark::defaultPreset

  ttk::label .lfSettings.lProfile
  ttk::combobox .lfSettings.cbProfile -value $::benchmark::profiles -textvariable $profileVarName
  .lfSettings.cbProfile set $::benchmark::defaultProfile

  ttk::label .lfSettings.lTune
  ttk::combobox .lfSettings.cbTune -value $::benchmark::tunes -textvariable $tuneVarName
  .lfSettings.cbTune set $::benchmark::defaultTune

  ttk::label .lfSettings.lParams
  ttk::entry .lfSettings.eParams -textvariable $paramsVarName
  set params $::benchmark::defaultOptions

  set pady {4 0}

  grid .lfSettings.lBitrate .lfSettings.sbBitrate -sticky news -pady $pady
  grid .lfSettings.lKeyInt  .lfSettings.sbKeyInt  -sticky news -pady $pady
  grid .lfSettings.lPreset  .lfSettings.cbPreset  -sticky news -pady $pady
  grid .lfSettings.lProfile .lfSettings.cbProfile -sticky news -pady $pady
  grid .lfSettings.lTune    .lfSettings.cbTune    -sticky news -pady $pady
  grid .lfSettings.lParams  .lfSettings.eParams   -sticky news -pady $pady

  grid columnconfigure .lfSettings 1 -weight 1
}

proc ::benchmark::changeWidgetsState {state} {
  foreach widget {
    .lfBenchmark.bRun
    .lfSettings.sbBitrate
    .lfSettings.sbKeyInt
    .lfSettings.cbPreset
    .lfSettings.cbProfile
    .lfSettings.cbTune
    .lfSettings.eParams
  } {
    $widget state $state
  }
}
