(in-package #:cl-user)

(ql:quickload "cl-x264-tune")

(defun build ()
  (sb-ext:save-lisp-and-die
   "benchmark.exe"
   :executable t
   :purify t
   :toplevel #'cl-x264-tune::main))
